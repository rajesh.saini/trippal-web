import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HomePageService {

  constructor(
    public http: Http
  ) { }

  fetchState(position) {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude
      + ',' + position.coords.longitude + '&key=AIzaSyB7m_iG9oq1LSN8CkqRMaxwHZ3wmEYFM7E')
      .map(res => {
        return res.json();
      });
  }

}
