import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageService } from './services/home-page.service';
import { HttpModule, Http, JsonpModule } from '@angular/http';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [],
  providers: [HomePageService]
})
export class CoreModule { }
