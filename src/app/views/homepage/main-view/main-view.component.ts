import { Component, OnInit, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { HomePageService } from '../../../core/services/home-page.service';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

declare var jQuery: any;
declare var WOW: any;

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit, AfterViewInit {

  @Output() navigateTo = new EventEmitter();
  public toCity = '';
  public fromCity = '';

  constructor(
    public http: Http,
    private _sanitizer: DomSanitizer,
    private homePageService: HomePageService
  ) { }

  ngOnInit() {
    this.getLocation();
    jQuery('#myModal').on('shown.bs.modal', function () {
      jQuery('#myInput').focus();
    });
    jQuery('.mdb-select').material_select();
  }

  ngAfterViewInit() {
    new WOW().init();
  }

  navigateToContent(id) {
    this.navigateTo.emit({ id: id });
  }

  observableSource = (keyword: any): Observable<any[]> => {
    if (keyword) {
      return this.http.get('https://trippalapis.herokuapp.com/rest/autocomplete/places?str=' + keyword + '&region=2')
        .map(res => {
          let json = [];
          if (res.json().hasOwnProperty('destinations') && res.json().destinations[0]
            && res.json().destinations[0].hasOwnProperty('cities')) {
            json = res.json().destinations[0].cities;
          }
          return json;
        });
    } else {
      return Observable.of();
    }
  }

  autocompleListFormatter = (data: any): SafeHtml => {
    const html = `<span>${data.destination}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  getCityState = function (resp) {
    let res = '';
    if (resp.status === 'OK') {
      if (resp.results[1]) {
        let city = false, state = false;
        for (let i = 0; i < resp.results.length; i++) {
          if ((!city || !state) && resp.results[i].types[0] === 'locality') {
            city = resp.results[i].address_components[0].short_name,
              state = resp.results[i].address_components[2].short_name;
            res = city + ', ' + state;
          }
        }
      }
    }
    return res;
  };


  getLocation() {
    navigator.geolocation.getCurrentPosition(position => {
      this.homePageService.fetchState(position).subscribe(res => {
        this.fromCity = this.getCityState(res);
      });
    });
  }
}
