import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  public lat = 26.9124;
  public lng = 75.7873;
  constructor() { }

  ngOnInit() {
  }

  showContent(e) {
    const filterItem = jQuery(e.id).offset().top;
    jQuery('html, body').animate({
      scrollTop: (filterItem - 90)
    }, 800);
  }

}
